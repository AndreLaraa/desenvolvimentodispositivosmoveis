package com.example.loginagenda

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_create_user.*

class CreateUserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_user)

        buttonCreate.setOnClickListener {
            create()
        }
    }

    private fun create() {
        val pass = editTextPass.text.toString()
        val email = editTextEmail.text.toString()
        val name = editTextName.text.toString()

        if(name.isEmpty()) {
            editTextName.requestFocus()
            return
        }

        if(email.isEmpty()) {
            editTextEmail.requestFocus()
            return
        }

        if(pass.isEmpty()) {
            editTextPass.requestFocus()
            return
        }

        Model.createUser(name = name, email = email, password = pass) {
            println("Retorno do createUser $it")
            if (it) {
                val intent = Intent(this, AgendaListActivity::class.java)
                startActivity(intent)
            } else {
                // LOGIN FALHA
            }
        }
    }
}
