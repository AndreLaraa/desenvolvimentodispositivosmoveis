package com.example.loginagenda

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import kotlinx.android.synthetic.main.activity_agenda_list.*

class AgendaListActivity : AppCompatActivity() {
    private lateinit var listView : ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agenda_list)

        confirmAddAgendaItem.setOnClickListener { addItem() }
    }

    override fun onResume() {
        super.onResume()
        var agendaItems = Model.current_user!!.agendaItems
        listView = findViewById<ListView>(R.id.listViewAgenda)

        val listItems = arrayOfNulls<String>(agendaItems.size)

        for (i in 0 until agendaItems.size) {
            val item = agendaItems[i]
            listItems[i] = item.name
        }

        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems)
        listView.adapter = adapter

        listView.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val intent = Intent(this, EditItemActivity::class.java)
            intent.putExtra("ITEM_POSITION", position);
            startActivity(intent)
        }
    }

    fun addItem(){
        val intent = Intent(this, AddItemActivity::class.java)
        startActivity(intent)
    }
}
