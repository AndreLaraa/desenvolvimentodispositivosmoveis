package com.example.loginagenda

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_edit_item.*
//import sun.jvm.hotspot.utilities.IntArray


class EditItemActivity : AppCompatActivity() {
    private lateinit var spinnerType : Spinner
    private val typeOptions = arrayOf("Casa","Trabalho", "Outro")
    private var position : Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_item)


        confirmEditionButton.setOnClickListener {
            confirmEditing()
        }

        cancelEditingAgendaItem.setOnClickListener {
            cancelEditing()
        }

        deleteAgendaItemButton.setOnClickListener {
            deleteItem()
        }

        position = intent.getIntExtra("ITEM_POSITION",-1)
        val agendaItem = Model.current_user!!.agendaItems[position]

        editTextAddress.setText(agendaItem.address)
        editTextName.setText(agendaItem.name)
        editTextPhone.setText(agendaItem.phone)

        val spinner = findViewById<Spinner>(R.id.editItemSpinner)
        if (spinner != null) {
            spinnerType = spinner
            val adapter = ArrayAdapter(this,
                R.layout.support_simple_spinner_dropdown_item,
                typeOptions )

            val position = adapter.getPosition(agendaItem.type)
            spinnerType.adapter = adapter
            spinnerType.setSelection(position)
        }

    }

    fun confirmEditing(){
        Model.current_user!!.agendaItems[position].name = editTextName.text.toString()
        Model.current_user!!.agendaItems[position].address = editTextAddress.text.toString()
        Model.current_user!!.agendaItems[position].phone = editTextPhone.text.toString()
        Model.current_user!!.agendaItems[position].type = spinnerType.selectedItem.toString()

        DataBaseService.updateInfos {
            finish()
        }

    }

    fun cancelEditing(){
        finish()
    }

    fun deleteItem(){
        DataBaseService.deleteItem(Model.current_user!!.email, Model.current_user!!.agendaItems[position]) {
            if (it) {
                val toast = Toast.makeText(applicationContext, "Sucesso ao deletar", Toast.LENGTH_SHORT)
                toast.view.backgroundTintList = ColorStateList.valueOf(Color.GREEN)
                toast.show()
                finish()
            } else {
                val toast = Toast.makeText(applicationContext, "Falha ao deletar", Toast.LENGTH_SHORT)
                toast.view.backgroundTintList = ColorStateList.valueOf(Color.RED)
                toast.show()
            }
        }
    }
}
