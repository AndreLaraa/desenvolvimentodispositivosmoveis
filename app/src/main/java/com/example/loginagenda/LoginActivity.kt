package com.example.loginagenda

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        signInButton.setOnClickListener {
            login()
        }

        signOnButton.setOnClickListener {
            signOn()
        }

    }

    private fun login(){
        val pass = editTextEmail.text.toString()
        val email = editTextName.text.toString()

        if(email.isEmpty()) {
            editTextName.requestFocus()
            return
        }

        if(pass.isEmpty()) {
            editTextEmail.requestFocus()
            return
        }

        Model.signIn(email = email, password = pass) {
            if (it) {
                val intent = Intent(this, AgendaListActivity::class.java)
                startActivity(intent)
            } else {
                // LOGIN FALHA
            }
        }
    }

    private fun signOn(){
        val intent = Intent(this, CreateUserActivity::class.java)
        startActivity(intent)
    }
}
