package com.example.loginagenda

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import org.json.JSONArray
import org.json.JSONObject
import java.io.File

data class AgendaItem(var name: String = "", var address: String = "", var phone: String = "", var type: String = "") {
    fun toJSON() : String {
        return "{'name':'$name', 'address':'$address', 'phone':'$phone', 'type':'$type'}"
    }
}
data class User(var name: String = "", var email: String = "", var password: String = "", var agendaItems: Array<AgendaItem> = emptyArray<AgendaItem>()) {
    fun toJSON() : String {
        val items = agendaItems.map { it.toJSON() }
        return "{'email':'$email', 'name':'$name', 'password':'$password', 'items': ${items}}"
    }
}

class Model {

    companion object {
        var current_user : User? = null;

        init {}

        fun createUser(name: String, email: String, password: String, callback: (sucess: Boolean) -> Unit) {
            // Primeiro tenta fazer login
            val newUser = User(name, email, password)
            DataBaseService.createUser(user = newUser) {
                if (it) {
                    println("createdUser ok")
                    current_user = newUser
                    callback(true)
                } else {
                    println("createdUser FALSO")
                    callback(false)
                }
            }

        }

        fun signIn(email: String, password: String, callback: (user: Boolean) -> Unit) {
            // Serviço para recuperar dados do email
            return DataBaseService.getUser(email = email) { userFromDB ->
                println(userFromDB)
                if (userFromDB != null && userFromDB.password == password) {
                    current_user = userFromDB
                    callback(true)
                } else {
                    callback(false);
                }
            }

        }

        fun saveNewItems(callback: (Boolean) -> Unit){

        }

    }

}

class DataBaseService {

    companion object {
        val fileName = "database.txt"
        var users : Array<User> = emptyArray()
        lateinit var fileObject : File

        init { }

        private fun getWritePermission(context: Context, activity: Activity){

            try {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions (activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                }

            } catch (e: Exception)  {
                print("PERMISSION ERROR: ${e.toString()}")
            }

        }

        fun initializeDatabase(context: Context, activity: Activity){
            getWritePermission(context, activity)

            val directory : File = context.filesDir
            fileObject = File(directory, fileName)

            val isNewFileCreated :Boolean = fileObject.createNewFile()
            if(isNewFileCreated){
                val items = arrayOf(AgendaItem("primeiro", "abc", "123","casa"), AgendaItem("segundo", "abc", "123","casa"))
                val default = User(name = "Administrador", email = "adm@email.com", password = "123", agendaItems = items )

                fileObject.writeText("{'users':[ ${ default.toJSON() } ]}")
                println("\n$fileName is created successfully with ${default.toJSON()}")
            } else println("\n$fileName already exists.")

            loadInfosFromFile()
        }

        private fun loadInfosFromFile(){
            val lines: List<String> = fileObject.readLines()
            var base = ""
            for (l in lines) { base += l }
            println(base)

            var usersJson : JSONArray = JSONObject(base)["users"] as  JSONArray
            usersJson = usersJson.get(0) as JSONArray

            for (i in 0 until usersJson.length()) {
                println(usersJson.get(i).toString())
                val userItem = usersJson.getJSONObject(i)
                val itemsAgendaJson : JSONArray = userItem["items"] as JSONArray
                var itemsAgenda : Array<AgendaItem> = emptyArray()

                for (j in 0 until itemsAgendaJson.length()) {
                    val agendaItem = itemsAgendaJson.getJSONObject(j)
                    itemsAgenda += AgendaItem(name = agendaItem["name"] as String, address = agendaItem["address"] as String, phone = agendaItem["phone"]  as String, type = agendaItem["type"] as String )
                }

                users += User(name = userItem["name"] as String, email = userItem["email"] as String, password = userItem["password"] as String, agendaItems = itemsAgenda )
            }
        }

        fun createUser(user: User, callback: (success: Boolean) -> Unit) {
            val alreadyExists = users.singleOrNull {  it.email == user.email }

            return if (alreadyExists == null) {
                users += user
                println("createUser sucesso  ")
                setFileByUsers { result ->
                    println("createUser retornando  ")
                    callback(result)
                }
            } else {
                callback(false)
                println("createUser erro  ")
            }
        }

        fun getUser(email: String, callback: (user: User?) -> Unit){
            return callback(users.singleOrNull {  it.email == email })
        }

        fun deleteItem(email: String, item: AgendaItem, callback: (success: Boolean) -> Unit){
            val getUserIndex = users.indexOfFirst {  it.email == email }
            val agendaSemItem = users[getUserIndex].agendaItems.filter {  it.name != item.name }

            if (users[getUserIndex].agendaItems.count() != agendaSemItem.count()) {
                users[getUserIndex].agendaItems = agendaSemItem.toTypedArray()

                setFileByUsers { result ->
                    callback(result)
                }
            } else {
                callback(false)
            }

        }

        fun addItem(item: AgendaItem, callback: (success: Boolean) -> Unit){
            val getUserIndex = users.indexOfFirst {  it.email == Model.current_user!!.email }

            users[getUserIndex].agendaItems += item

            setFileByUsers { result ->
                callback(result)
            }
        }

        fun updateInfos(callback: (success: Boolean) ->Unit) {
            setFileByUsers { result ->
                callback(result)
            }
        }

        private fun setFileByUsers(callback: (success: Boolean) -> Unit ) {
            return try {
                // replace all content by users variable
                fileObject.writeText("{'users':[ ${ users.map { it.toJSON() } } ]}")
                println("Alterado o database ")
                callback(true)
            } catch (e: Exception)  {
                println("setFileByUsers ERROR: ${e.toString()}")
                callback(false)
            }
        }
    }
}