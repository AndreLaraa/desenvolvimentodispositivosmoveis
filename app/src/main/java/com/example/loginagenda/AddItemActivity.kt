package com.example.loginagenda

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_item.*

class AddItemActivity : AppCompatActivity() {

    private val typeOptions = arrayOf("Casa","Trabalho", "Outro")
    private lateinit var spinnerType : Spinner
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_item)

        val spinner = findViewById<Spinner>(R.id.addItemSpinner)
        if (spinner != null) {
            spinnerType = spinner
            val adapter = ArrayAdapter(this,
                R.layout.support_simple_spinner_dropdown_item,
                typeOptions )
            spinnerType.adapter = adapter
            spinnerType.setSelection(1)
        }

        confirmAddAgendaItem.setOnClickListener { addNewItem() }
    }

    private fun addNewItem(){
        val name = editTextName.text.toString()
        val address = editTextAddress.text.toString()
        val phone = editTextPhone.text.toString()
        val type = spinnerType.selectedItem.toString()

        if(name.isEmpty()) {
            editTextName.requestFocus()
            return
        }

        if(address.isEmpty()) {
            editTextAddress.requestFocus()
            return
        }

        if(phone.isEmpty()) {
            editTextPhone.requestFocus()
            return
        }

        val new_item = AgendaItem(name, address, phone, type)
        DataBaseService.addItem(new_item) {
            if (it) {
                Toast.makeText(applicationContext, "Sucesso", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(applicationContext, "Falha ao adicionar", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
